$(document).ready(function(){

    shuffleCards();
    placeCards();

    var cardsClicked = 0;

    $('.card').on('click', function(){

        if($(this).hasClass('selected') || $(this).hasClass('correct')){
            return;
        }

        $(this).addClass('selected');
        cardsClicked++;
        if(cardsClicked === 2){
            cardsClicked = 0;
            compareCards();
        }

    });

});

var cards = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6];

function placeCards(){
    var currentCard;
    while(cards.length > 0){
        currentCard = cards.shift();
        $('#canvas').append('<div class="card color-' + currentCard + '"></div>');
    }
}

function shuffle(o) {
    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

function shuffleCards(){
    cards = shuffle(cards);
}

function compareCards(){
    var card1 = $('.card.selected').first();
    var card2 = $('.card.selected').last();

    if(card1.attr('class') === card2.attr('class')){
        card1.addClass('correct').removeClass('selected');
        card2.addClass('correct').removeClass('selected');
    }
    else {
        setTimeout(function(){
            card1.removeClass('selected');
            card2.removeClass('selected');
        }, 500);
    }
}